#
# Cookbook Name:: tim-preferences
# Recipe:: vim
#
# Copyright 2014, Stitchy, LLC
#
# All rights reserved - Do Not Redistribute
#

package "vim"

cookbook_file "vimrc" do
  path "#{node["tim-preferences"]["home-prefix"]}#{node["tim-preferences"]["owner"]}/.vimrc"
  owner node["tim-preferences"]["owner"]
  group node["tim-preferences"]["group"]
  mode "0644"
end

directory "#{node["tim-preferences"]["home-prefix"]}#{node["tim-preferences"]["owner"]}/.vim/colors" do
  recursive true
end

cookbook_file "vim-color-profile.vim" do
  path "#{node["tim-preferences"]["home-prefix"]}#{node["tim-preferences"]["owner"]}/.vim/colors/tim-blue.vim"
  owner node["tim-preferences"]["owner"]
  group node["tim-preferences"]["group"]
  mode "0644"
end

