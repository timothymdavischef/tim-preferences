#
# Cookbook Name:: tim-preferences
# Recipe:: terminal
#
# Copyright 2014, Stitchy, LLC
#
# All rights reserved - Do Not Redistribute
#

execute "load-gnome-terminal-configuration" do
  command "gconftool-2 --load /etc/gnome/gnome-terminal-conf.xml"
  user node["tim-preferences"]["owner"]
  action :nothing
end

directory "/etc/gnome/" do
  recursive true
end

cookbook_file "gnome-terminal-conf.xml" do
  path "/etc/gnome/gnome-terminal-conf.xml"
  mode "0644"
  notifies :run, "execute[load-gnome-terminal-configuration]", :immediately
end
