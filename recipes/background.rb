#
# Cookbook Name:: tim-preferences
# Recipe:: background
#
# Copyright 2014, Stitchy, LLC
#
# All rights reserved - Do Not Redistribute
#

directory File.expand_path "..", node["tim-preferences"]["image-destination"] do
  mode 00755
  recursive true
end

execute "set-background" do
  case node["platform_family"]
  when "mac_os_x"
    command "defaults write com.apple.desktop Background '{default = {ImageFilePath = \"#{node["tim-preferences"]["image-destination"]}\"; };}'"
  when "debian", "fedora"
    if has_gnome_shell
      command "gsettings set org.gnome.desktop.background picture-uri file://#{node["tim-preferences"]["image-destination"]}"
    end
  end
  user node["tim-preferences"]["owner"]
  action :nothing
end

cookbook_file node["tim-preferences"]["image-source"] do
  path node["tim-preferences"]["image-destination"]
  mode 00644
  notifies :run, "execute[set-background]", :immediately
end

