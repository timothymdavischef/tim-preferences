#
# Cookbook Name:: tim-preferences
# Recipe:: default
#
# Copyright 2014, Stitchy, LLC
#
# All rights reserved - Do Not Redistribute
#

case node["platform_family"]
when "mac_os_x"
  include_recipe "homebrew"
end

include_recipe "tim-preferences::vim"
include_recipe "tim-preferences::background"

if has_gnome_shell
  include_recipe "tim-preferences::terminal"
end

user_configuration_files = [
  "bash_profile",
  "bashrc",
  "bashrc_git",
  "bashrc_marks",
  "bashrc_export",
  "screenrc"
]

user_configuration_files.each do |configuration_file|
  cookbook_file configuration_file do
    path "#{node["tim-preferences"]["home-prefix"]}#{node["tim-preferences"]["owner"]}/.#{configuration_file}"
    owner node["tim-preferences"]["owner"]
    group node["tim-preferences"]["group"]
    mode "0644"
  end
end

case node['platform_family']
when 'mac_os_x'
  node["tim-preferences"]["osx-defaults"].each_pair do |key, value|
    current_value = `su #{node["tim-preferences"]["owner"]} -c "defaults read #{key}"`
    current_value = current_value.strip
    if key.include? "-bool"
      # Hack for boolean values, which are read as 1 or 0 for some reason
      current_value = current_value == "1" ? "true" : current_value;
      current_value = current_value == "0" ? "false" : current_value;
    end
    unless current_value.to_s.include? value.to_s
      execute "set-#{key}" do
        command "defaults write #{key} #{value}"
        user node["tim-preferences"]["owner"]
      end
    end
  end
end

if has_gnome_shell
  node["tim-preferences"]["gsettings"].each_pair do |key, value|
    current_value = `su #{node["tim-preferences"]["owner"]} -c "gsettings get #{key}"`
    unless current_value.to_s.include? value.to_s
      execute "set-#{key}" do
        command "gsettings set #{key} #{value}"
        user node["tim-preferences"]["owner"]
      end
    end
  end
end
