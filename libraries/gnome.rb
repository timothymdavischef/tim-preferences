class Chef::Recipe
  def has_gnome_shell()
    gnome_shell_version = `gnome-shell --version 2>&1`
    return gnome_shell_version.include? "GNOME Shell"
  end
end
