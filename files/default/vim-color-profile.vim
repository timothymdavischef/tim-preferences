" Vim color file {{{1
"  Maintainer: hira@users.sourceforge.jp
" Last Change: 2003/11/08 (Sat) 15:09:08.
"     Version: 1.3
" This color scheme uses a dark background.

" Options
" format:
"        hhcs_<gui|cterm|both>_<theme|all>_<target>="value"
" target:
"    linenr
"        "light"      : underline, fg=black,        bg=light<theme>
"        "dark"       : underline, fg=black,        bg=dark<theme>
"        "normal"     : none,      fg=light<theme>, bg=black
"    nontext
"        "underlined" : underline, fg=dark<theme>,  bg=black
"        "normal"     : none,      fg=dark<theme>,  bg=black
"    
"        This is original settings.
"            :let hhcs_both_all_linenr  ="light"
"            :let hhcs_both_all_nontext ="underlined"
"    
"        This is prototype settings.
"            :let hhcs_both_all_linenr  ="dark"
"            :let hhcs_both_all_nontext ="underlined"
"    
"        If you don't want to eccentric feature, try this.
"            :let hhcs_both_all_linenr  ="normal"
"            :let hhcs_both_all_nontext ="normal"
"
"        Normal, except hhdgray(gui), hhdgreen(cterm).
"            :let hhcs_both_all_linenr        ="normal"
"            :let hhcs_both_all_nontext       ="normal"
"            :let hhcs_gui_hhdgray_linenr     ="light"
"            :let hhcs_gui_hhdgray_nontext    ="underlined"
"            :let hhcs_cterm_hhdgreen_linenr  ="dark"
"            :let hhcs_cterm_hhdgreen_nontext ="underlined"
"
"        For poor cterm
"            :let hhcs_cterm_all_linenr     ="normal"

" Happy Hacking color scheme ((DARK)) {{{1
set background=dark
hi clear
if exists("syntax_on")
   syntax reset
endif
let colors_name       = expand("<sfile>:t:r")
let html_my_rendering = 1


" frame & title & message (theme) {{{1
hi NonText                   gui=NONE        guifg=darkblue       guibg=NONE
hi NonText                 cterm=NONE      ctermfg=darkblue     ctermbg=NONE
hi Folded                    gui=NONE        guifg=blue           guibg=NONE
hi Folded                  cterm=NONE      ctermfg=blue         ctermbg=NONE
hi FoldColumn                gui=NONE        guifg=blue           guibg=NONE
hi FoldColumn              cterm=NONE      ctermfg=blue         ctermbg=NONE
hi StatusLineNC              gui=NONE        guifg=black          guibg=blue
hi StatusLineNC              gui=NONE      ctermfg=black        ctermbg=blue
hi LineNr                    gui=NONE        guifg=black          guibg=lightblue
hi LineNr                  cterm=NONE      ctermfg=black        ctermbg=lightblue
hi VertSplit                 gui=NONE        guifg=darkblue       guibg=darkblue
hi VertSplit               cterm=NONE      ctermfg=darkblue     ctermbg=darkblue
" title
hi Title                     gui=NONE        guifg=lightgray      guibg=darkblue
hi Title                   cterm=NONE      ctermfg=lightgray    ctermbg=darkblue
" message
hi MoreMsg                   gui=NONE        guifg=black          guibg=blue
hi MoreMsg                 cterm=NONE      ctermfg=black        ctermbg=blue
hi Question                  gui=NONE        guifg=black          guibg=blue
hi Question                cterm=NONE      ctermfg=black        ctermbg=blue


" cursor {{{1
hi StatusLine                gui=NONE   guifg=black          guibg=gray
hi StatusLine              cterm=NONE ctermfg=black        ctermbg=gray
hi WildMenu                  gui=NONE   guifg=black          guibg=green
hi WildMenu                cterm=NONE ctermfg=black        ctermbg=green
hi Cursor                    gui=NONE   guifg=black          guibg=green
hi Cursor                  cterm=NONE ctermfg=black        ctermbg=green
hi IncSearch                 gui=NONE   guifg=black          guibg=green
hi IncSearch               cterm=NONE ctermfg=black        ctermbg=green
hi CursorIM                  gui=NONE   guifg=black          guibg=green
hi CursorIM                cterm=NONE ctermfg=black        ctermbg=green
hi Search                    gui=NONE   guifg=black          guibg=yellow
hi Search                  cterm=NONE ctermfg=black        ctermbg=yellow
hi Visual                    gui=NONE   guifg=black          guibg=gray
hi Visual                  cterm=NONE ctermfg=black        ctermbg=gray


" message {{{1
hi ErrorMsg                  gui=NONE   guifg=black          guibg=red
hi ErrorMsg                cterm=NONE ctermfg=black        ctermbg=red
hi WarningMsg                gui=NONE   guifg=black          guibg=yellow
hi WarningMsg              cterm=NONE ctermfg=black        ctermbg=yellow
hi ModeMsg                   gui=NONE   guifg=black          guibg=green
hi ModeMsg                 cterm=NONE ctermfg=black        ctermbg=green


" inner {{{1
hi Normal                    gui=NONE        guifg=lightgray      guibg=NONE
hi Normal                  cterm=NONE      ctermfg=lightgray    ctermbg=NONE
hi Ignore                    gui=NONE        guifg=black          guibg=NONE
hi Ignore                  cterm=NONE      ctermfg=black        ctermbg=NONE
hi Todo                      gui=NONE   guifg=black          guibg=red
hi Todo                    cterm=NONE ctermfg=black        ctermbg=red
hi Error                     gui=NONE   guifg=lightgray      guibg=red
hi Error                   cterm=NONE ctermfg=lightgray    ctermbg=red
hi Special                   gui=NONE        guifg=lightcyan      guibg=NONE
hi Special                 cterm=NONE      ctermfg=lightcyan    ctermbg=NONE
hi SpecialKey                gui=NONE        guifg=cyan           guibg=NONE
hi SpecialKey              cterm=NONE      ctermfg=cyan         ctermbg=NONE
hi Identifier                gui=NONE        guifg=cyan           guibg=NONE
hi Identifier              cterm=NONE      ctermfg=cyan         ctermbg=NONE
hi Constant                  gui=NONE        guifg=lightred       guibg=NONE
hi Constant                cterm=NONE      ctermfg=lightred     ctermbg=NONE
hi Statement                 gui=NONE        guifg=lightyellow    guibg=NONE
hi Statement               cterm=NONE      ctermfg=lightyellow  ctermbg=NONE
hi Comment                   gui=NONE        guifg=lightblue      guibg=NONE
hi Comment                 cterm=NONE      ctermfg=lightblue    ctermbg=NONE
hi Underlined                gui=NONE   guifg=lightblue      guibg=NONE
hi Underlined              cterm=NONE ctermfg=lightblue    ctermbg=NONE
hi Directory                 gui=NONE        guifg=lightgreen     guibg=NONE
hi Directory               cterm=NONE      ctermfg=lightgreen   ctermbg=NONE
hi PreProc                   gui=NONE        guifg=lightmagenta   guibg=NONE
hi PreProc                 cterm=NONE      ctermfg=lightmagenta ctermbg=NONE
hi Type                      gui=NONE        guifg=lightgreen     guibg=NONE
hi Type                    cterm=NONE      ctermfg=lightgreen   ctermbg=NONE


" option {{{1
function! s:SetOpt(term, theme, target, default)
    let s:opt_{a:term}_{a:target}
    \    = exists("g:hhcs_".a:term."_".a:theme."_".a:target)
    \        ? g:hhcs_{a:term}_{a:theme}_{a:target}
    \    : exists("g:hhcs_".a:term."_all_".a:target)
    \        ? g:hhcs_{a:term}_all_{a:target}
    \    : exists("g:hhcs_both_all_".a:target)
    \        ? g:hhcs_both_all_{a:target}
    \        : a:default
endfunction


" LineNr {{{2
" light, dark, normal
call s:SetOpt(  "gui", "hhdblue", "linenr", "light")
call s:SetOpt("cterm", "hhdblue", "linenr", "light")
"echo "s:opt_gui_linenr=".s:opt_gui_linenr
if s:opt_gui_linenr == "light"
    hi LineNr gui=NONE guifg=black guibg=lightblue
elseif s:opt_gui_linenr == "dark"
    hi LineNr gui=NONE guifg=black guibg=darkblue
else
    hi LineNr gui=NONE guifg=lightblue guibg=NONE
endif
"echo "s:opt_cterm_linenr=".s:opt_cterm_linenr
if s:opt_cterm_linenr == "light"
    hi LineNr cterm=NONE ctermfg=black ctermbg=lightblue
elseif s:opt_gui_linenr == "dark"
    hi LineNr cterm=NONE ctermfg=black ctermbg=darkblue
else
    hi LineNr cterm=NONE ctermfg=lightblue ctermbg=NONE
endif


" NonText {{{2
" underlined, normal
call s:SetOpt(  "gui", "hhdblue", "nontext", "underlined")
call s:SetOpt("cterm", "hhdblue", "nontext", "underlined")
"echo "s:opt_gui_nontext=".s:opt_gui_nontext
if s:opt_gui_nontext == "underlined"
    hi NonText gui=NONE guifg=darkblue guibg=NONE
else
    hi NonText gui=NONE guifg=darkblue guibg=NONE
endif
"echo "s:opt_cterm_nontext=".s:opt_cterm_nontext
if s:opt_cterm_nontext == "underlined"
    hi NonText cterm=NONE ctermfg=darkblue ctermbg=NONE
else
    hi NonText cterm=NONE ctermfg=darkblue ctermbg=NONE
endif


" 2}}}
" diff {{{1
hi DiffText                  gui=NONE   guifg=black          guibg=red
hi DiffText                cterm=NONE ctermfg=black        ctermbg=red
hi DiffChange                gui=NONE   guifg=black          guibg=lightgray
hi DiffChange              cterm=NONE ctermfg=black        ctermbg=lightgray
hi DiffDelete                gui=NONE        guifg=black          guibg=blue
hi DiffDelete              cterm=NONE      ctermfg=black        ctermbg=blue
hi DiffAdd                   gui=NONE   guifg=black          guibg=cyan
hi DiffAdd                 cterm=NONE ctermfg=black        ctermbg=cyan


" html {{{1
hi htmlLink                  gui=NONE   guifg=lightblue      guibg=NONE
hi htmlLink                cterm=NONE ctermfg=lightblue    ctermbg=NONE
hi htmlBold                  gui=NONE   guifg=black          guibg=lightred
hi htmlBold                cterm=NONE ctermfg=black        ctermbg=lightred
hi htmlItalic                gui=NONE   guifg=black          guibg=lightgreen
hi htmlItalic              cterm=NONE ctermfg=black        ctermbg=lightgreen
hi htmlBoldItalic            gui=NONE   guifg=black          guibg=lightblue 
hi htmlBoldItalic          cterm=NONE ctermfg=black        ctermbg=lightblue 
hi htmlUnderline             gui=NONE   guifg=lightgray      guibg=NONE
hi htmlUnderline           cterm=NONE ctermfg=lightgray    ctermbg=NONE
hi htmlBoldUnderline         gui=NONE   guifg=lightred       guibg=NONE
hi htmlBoldUnderline       cterm=NONE ctermfg=lightred     ctermbg=NONE
hi htmlUnderlineItalic       gui=NONE   guifg=lightgreen     guibg=NONE
hi htmlUnderlineItalic     cterm=NONE ctermfg=lightgreen   ctermbg=NONE
hi htmlBoldUnderlineItalic   gui=NONE   guifg=lightblue      guibg=NONE
hi htmlBoldUnderlineItalic cterm=NONE ctermfg=lightblue    ctermbg=NONE


" colors{{{1
"     1  black
"     2  blue
"     3  cyan
"     4  darkblue
"     5  gray
"     6  green
"     7  lightblue
"     8  lightcyan
"     9  lightgray
"    10  lightgreen
"    11  lightmagenta
"    12  lightred
"    13  lightyellow
"    14  red
"    15  yellow
"}}}1
" vim:set nowrap foldmethod=marker expandtab:
