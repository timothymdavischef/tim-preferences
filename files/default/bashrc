# .bashrc
# This script is run for all interactive non-login shells. For login shells see .bash_profile.

# load the default bashrc first so that this script overrides anything in it.
if [ -f /etc/bashrc ]; then
	source /etc/bashrc
fi

# if the git prompt configuration exists, load it.
if [ -f ~/.bashrc_git ]; then
	source ~/.bashrc_git
fi

# if the marks configuration exists, load it.
if [ -f ~/.bashrc_marks ]; then
	source ~/.bashrc_marks
fi

# if the export configuration exists, load it.
if [ -f ~/.bashrc_export ]; then
	source ~/.bashrc_export
fi

if ls --color > /dev/null 2>&1; then # GNU `ls`
   colorflag="--color"
else # OS X `ls`
   colorflag="-G"
fi

alias l="ls"
alias ls="ls $colorflag"
alias ll="ls -l"
alias la="ls -a"
alias vi="vim"

PS1="\e[0;33m\h\e[0;30m:\e[0;32m\u\e[0;30m:\e[0;36m\w\e[m"
if [[ "$(type -t __git_ps1)" == "function" ]]
then
   GIT_PS1_SHOWDIRTYSTATE=true
   PS1=$PS1'\e[0;37m$(__git_ps1 " (%s)")\e[m'
fi
export PS1="$PS1\n$ "

# set vi command line mode
set -o vi

# ignore wall messages from everyone except root
mesg n

# If rlwrap is installed and the auto-complete file exists.
rlwrap -v &>/dev/null
if [[ $? -eq 0 && -f $HOME/.local/share/sqlplus/auto-complete/auto-complete.dict ]]
then
   # Use rlwrap for autocomplete and history.
   alias sqlplus="rlwrap -b \"\" -f $HOME/.local/share/sqlplus/auto-complete/auto-complete.dict sqlplus"
fi
