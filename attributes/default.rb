default["tim-preferences"]["owner"] = "tim"
default["tim-preferences"]["group"] = "tim"
default["tim-preferences"]["home-prefix"] = "/home/"
default["tim-preferences"]["image-source"] = "background.jpg"
default["tim-preferences"]["image-destination"] = "/usr/share/backgrounds/chef/background.jpg"

# See gsettings-example.rb for more information.
default["tim-preferences"]["gsettings"] = {
  "org.gnome.desktop.interface clock-format" => "12h",
  "org.gnome.desktop.screensaver lock-enabled" => "true",
  "org.gnome.gedit.preferences.editor display-line-numbers" => "true",
  "org.gnome.gedit.preferences.editor bracket-matching" => "true",
  "org.gnome.login-screen disable-user-list" => "true",
  "org.gnome.nautilus.preferences always-use-location-entry" => "true",
  "org.gnome.nautilus.preferences enable-delete" => "true",
  "org.gnome.settings-daemon.peripherals.mouse locate-pointer" => "false",
  "org.gnome.settings-daemon.peripherals.touchpad horiz-scroll-enabled" => "true",
  "org.gnome.settings-daemon.peripherals.touchpad scroll-method" => "two-finger-scrolling",
  "org.gnome.settings-daemon.peripherals.touchpad tap-to-click" => "false"
}

# See https://raw.github.com/mathiasbynens/dotfiles/master/.osx for more
default["tim-preferences"]["osx-defaults"] = {
  # Only use UTF-8 in Terminal.app
  "com.apple.terminal StringEncodings -array" => "4",
  # Disable Dashboard
  "com.apple.dashboard mcx-disabled -bool" => "true",
  # Automatically hide and show the Dock
  "com.apple.dock autohide -bool" => "true",
  # Don’t show Dashboard as a Space
  "com.apple.dock dashboard-in-overlay -bool" => "true",
  # Set the icon size of Dock items to 36 pixels
  "com.apple.dock tilesize -int" => "36",
  # Change minimize/maximize window effect
  "com.apple.dock mineffect -string" => "scale",
  # Minimize windows into their application’s icon
  "com.apple.dock minimize-to-application -bool" => "true",
  # Wipe all (default) app icons from the Dock
  # This is only really useful when setting up a new Mac, or if you don’t use
  # the Dock to launch apps.
  "com.apple.dock persistent-apps -array" => "",
  # Top left screen corner → Mission Control
  "com.apple.dock wvous-tl-corner -int" => "2",
  "com.apple.dock wvous-tl-modifier -int" => "0",
  # Finder: allow quitting via ⌘ + Q; doing so will also hide desktop icons
  "com.apple.finder QuitMenuItem -bool" => "true",
  # Finder: show hidden files by default
  "com.apple.finder AppleShowAllFiles -bool" => "true",
  # Hide desktop icons
  "com.apple.finder CreateDesktop -bool" => "false",
  # Empty Trash securely by default
  "com.apple.finder EmptyTrashSecurely -bool" => "true",
  # Disable the warning when changing a file extension
  "com.apple.finder FXEnableExtensionChangeWarning -bool" => "false",
  # Finder: show path bar
  "com.apple.finder ShowPathbar -bool" => "true",
  # Disable the “Are you sure you want to open this application?” dialog
  "com.apple.LaunchServices LSQuarantine -bool" => "false",
  # Require password immediately after sleep or screen saver begins
  "com.apple.screensaver askForPassword -int" => "1",
  "com.apple.screensaver askForPasswordDelay -int" => "0",
  # Check for software updates daily, not just once per week
  "com.apple.SoftwareUpdate ScheduleFrequency -int" => "1",
  # Set highlight color to green
  "NSGlobalDomain AppleHighlightColor -string" => "0.764700\ 0.976500\ 0.568600",
  # Disable press-and-hold for keys in favor of key repeat
  "NSGlobalDomain ApplePressAndHoldEnabled -bool" => "false",
  # Finder: show all filename extensions
  "NSGlobalDomain AppleShowAllExtensions -bool" => "true",
  # Disable “natural” (Lion-style) scrolling
  "NSGlobalDomain com.apple.swipescrolldirection -bool" => "false",
  # Set a blazingly fast keyboard repeat rate
  "NSGlobalDomain KeyRepeat -int" => "0",
  # Save to disk (not to iCloud) by default
  "NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool" => "false",
  # Give Finder and a few other programs a flatter look
  "NSGlobalDomain NSUseLeopardWindowValues" => "NO"
}
